<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Parse Numbers Within Objects

## Table of Contents

*  [Overview](#overview)
*  [Installation Prerequisites](#installation-prerequisites)
*  [How to Install](#how-to-install)
*  [How to Run](#how-to-run)
*  [Attributes](#attributes)
*  [Examples](#examples)
*  [Additional Information](#additional-information)

## Overview

This JST allows IAP users to convert strings and strings within objects to numbers. Since the JST uses the JavaScript function [parseFloat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseFloat) to perform this type of conversion, conversion of strings with floating point numbers is also supported in addition to integers. The supported input types are: `string`, `object` and `array`. For arrays and objects, the JST will convert strings in the first level only (at this time, recursion is not yet supported by JST), all other levels will remain as is. In the case of an object or an array of objects, users can choose to specify the key-value pairs to be converted in all elements of the array. If not specified, all key-value pairs will attempt to be converted. Please note, if an invalid (non-number) string is passed to the JST, the JST will return the string as is without attempting the conversion rather than erroring out.

## Installation Prerequisites
Users must satisfy the following prerequisites:
* Itential Automation Platform : `^2022.1`

## How to Install

To install the Pre-Built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The Pre-Built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:
1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the section in your workflow where you would like to convert a string to an object and add a `JSON Transformation` task.

2. Inside the `Transformation` task, search for and select `parseNumberWithinObject` (the name of the internal JST).

3. After selecting the task, the transformation dialog will display.

4. The inputs to the JST would be the data that needs to be converted and the keys to be converted if the data is an object or an array of objects.

5. Save your input and the task is ready to run inside of IAP.

## Attributes  

Attributes for the Pre-Built are outlined in the following tables.

**Input**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>data</code></td>
<td>Data that needs to be converted to string.</td>
<td><code>string/number/integer/object/array</code></td>
</tr>
<tr>
<td><code>keysToConvert</code></td>
<td>The keys to be converted in the object if the data is an object or an array of objects. Set this attribute to <code>[]</code> if not applicable, or if all keys need to be converted.</td>
<td><code>array[string]</code></td>
</tr>
</tbody>
</table>

**Output**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>output</code></td>
<td>Converted data.</td>
<td><code>string/number/integer/object/array</code></td>
</tr>
</tbody>
</table>

## Examples

The following examples describe how the Pre-Built will work for different inputs.

### Example 1
**Input:** `string`

```
{
  "keysToConvert": [],
  "data": "32.23"
}
  ```

**Output**

```
32.23
  ```

### Example 2
**Input:** `array`

```
{
  "keysToConvert": [],
  "data": [12, true, "32.23", "banana", ["43"], "35"]
}
  ```

**Output**

```
[
  12,
  true,
  32.23,
  "banana",
  ["43"],
  35
]
  ```

### Example 3 
**Input:** `object` with empty `keysToConvert`

```
{
  "keysToConvert": [],
  "data": {
    "fruit": "apple",
    "quantity": "30",
    "price": "4",
    "aisle": "23"
  }
}
  ```

**Output**

```
{
  "fruit": "apple",
  "quantity": 30,
  "price": 4,
  "aisle": 23
}
  ```
  
### Example 4
  
**Input:** `object` with non-empty `keysToConvert`

```
{
  "keysToConvert": ["price","aisle"],
  "data": {
    "fruit": "apple",
    "quantity": "30",
    "price": "42.2",
    "aisle": "23b"
  }
}
  ```

**Output**
```
{
  "fruit": "apple",
  "quantity": "30",
  "price": 42.2,
  "aisle": "23b"
}
  ```

### Example 5
  
**Input:** `array` of `objects` with non-empty `keysToConvert`

```
{
  "keysToConvert": ["quantity","aisle"],
  "data": [
    {
      "fruit": "apple",
      "quantity": "30",
      "price": "42.2",
      "aisle": "23b"
    },
    {
      "fruit": "banana",
      "quantity": "89",
      "price": "10.1",
      "aisle": "23a"
    },
    {
      "fruit": "pineapple",
      "quantity": "32",
      "price": "20",
      "aisle": "21"
    }
  ]
}
  ```

**Output**

```
[
  {
    "fruit": "apple",
    "quantity": 30,
    "price": "42.2",
    "aisle": "23b"
  },
  {
    "fruit": "banana",
    "quantity": 89,
    "price": "10.1",
    "aisle": "23a"
  },
  {
    "fruit": "pineapple",
    "quantity": 32,
    "price": "20",
    "aisle": 21
  }
]
```

### Example 6

**Input:** `array` of `objects` with empty `keysToConvert`

```
{
  "keysToConvert": [],
  "data": [
    {
      "FILE_SYSTEM": "net/node0_RP0_CPU0/harddisk",
      "TOTAL_SIZE": "17538482176",
      "TOTAL_FREE": "14333425152"
    },
    {
      "FILE_SYSTEM": "net/node0_RP1_CPU0/harddisk",
      "TOTAL_SIZE": "17538482175",
      "TOTAL_FREE": "44222222"
    }
  ]
}
  ```

**Output**

```
[
  {
    "FILE_SYSTEM": "net/node0_RP0_CPU0/harddisk",
    "TOTAL_SIZE": 17538482176,
    "TOTAL_FREE": 14333425152
  },
  {
    "FILE_SYSTEM": "net/node0_RP1_CPU0/harddisk",
    "TOTAL_SIZE": 17538482175,
    "TOTAL_FREE": 44222222
  }
]
```

## Additional Information
Please use your Itential Customer Success account if you need support when using this Pre-Built Transformation.
