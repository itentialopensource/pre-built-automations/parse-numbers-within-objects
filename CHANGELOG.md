
## 0.0.7 [01-30-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/parse-numbers-within-objects!8

---

## 0.0.6 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/parse-numbers-within-objects!7

---

## 0.0.5 [06-09-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/parse-numbers-within-objects!6

---

## 0.0.4 [11-29-2021]

* Certification for 21.2

See merge request itentialopensource/pre-built-automations/parse-numbers-within-objects!5

---

## 0.0.3 [10-27-2021]

* Patch/dsup 958

See merge request itentialopensource/pre-built-automations/parse-numbers-within-objects!4

---

## 0.0.2 [10-27-2021]

* Certified for 2020.2

See merge request itentialopensource/pre-built-automations/parse-numbers-within-objects!3

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
